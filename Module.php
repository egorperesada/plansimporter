<?php

namespace perec\plans;

/**
 * plansImporter module definition class
 */
class Module extends \yii\base\Module
{

    public $controllerNamespace = 'perec\plans\controllers';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    
        $this->params["config"] = require_once ( $this->getBasePath() . "/config.php");
    }
}
