<?php

namespace perec\plans\models;

use Yii;

/**
 * This is the model class for table "plan_properties".
 *
 * @property int $property_id
 * @property int $property_type_id
 * @property string $active_from
 * @property string $active_to
 * @property int $plan_id
 * @property string $prop_value
 *
 * @property Plans $plan
 */
class PlanProperties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_type_id', 'plan_id'], 'required'],
            [['property_type_id', 'plan_id'], 'integer'],
            [['prop_value'], 'string'],
            [['active_from', 'active_to'], 'string', 'max' => 11],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::class, 'targetAttribute' => ['plan_id' => 'plan_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'property_id' => 'Property ID',
            'property_type_id' => 'Property Type ID',
            'active_from' => 'Active From',
            'active_to' => 'Active To',
            'plan_id' => 'Plan ID',
            'prop_value' => 'Prop Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plans::class, ['plan_id' => 'plan_id']);
    }

 

}
