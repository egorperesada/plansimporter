<?php
/**
 * Created by eaperesada.
 * Date: 29.05.2018
 * Time: 21:44
 * Email: ea.peresada@gmail.com
 * Phone: +7 (922) 339 60 49
 * VK: https://vk.com/peresada
 * Project: domrutest
 */

namespace perec\plans\services;


use perec\plans\models\MassLoad;
use yii\db\ActiveRecord;

class Plans extends MassLoad {
    
    public function __construct( ActiveRecord $model ) {
        parent::__construct( $model );
        $this->setOnDuplicateUpdateColumns( [ "plan_name" , "plan_group_id" , "active_to" , "company_id" ] );
    }
    
    /**
     * Validation rules for plans.xml ROWS
     *
     * @param array $data
     *
     * @return bool
     * */
    public function validateRow( array $data ) {
        if ( ! is_array( $data[ "active_to" ] ) ) {
            return $this->dateValidate( $data[ "active_to" ] );
        }
        else {
            return true;
        }
    }
    
    
}